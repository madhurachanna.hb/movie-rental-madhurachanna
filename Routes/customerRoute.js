const customersModel = require("../models/customer");
const express = require("express");
const customerApp = express.Router();

customerApp.get("/api/customers", (req, res) => {
  customersModel.find((err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.send(data);
  });
});

customerApp.post("/api/customers", (req, res) => {
  customersModel.create(req.body, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.status(201).send(data);
  });
});

customerApp.get("/api/customers/:customerId", (req, res) => {
  customersModel.findById(req.params.customerId, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.status(209).send(data);
  });
});

customerApp.put("/api/customers/:customerId", (req, res) => {
  movieModel.findByIdAndUpdate(req.params.customerId, req.body, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.status(201).send("Movie Modifieds");
  });
});

customerApp.delete("/api/customers/:customerId", (req, res) => {
  movieModel.findByIdAndDelete(req.params.customerId, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.send("Deleted Sucessfully");
  });
});

module.exports = customerApp;
