const movieModel = require("../models/movie");
const express = require("express");
const movieApp = express.Router();
const winston = require("winston");
const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "user-service" },
  transports: [
    new winston.transports.File({
      filename: "./Logs/movie.log"
    })
  ]
});
logger.add(
  new winston.transports.Console({
    format: winston.format.simple()
  })
);

movieApp.get("/api/movies", (req, res) => {
  movieModel.find((err, data) => {
    if (err) {
      logger.error(err.message);
      res.send(err.message);
    }
    res.send(data);
    logger.info("Requested data sent to the user");
  });
});

movieApp.post("/api/movies", (req, res) => {
  movieModel.create(req.body, (err, data) => {
    if (err) {
      logger.error(err.message);
      res.send(err.message);
    }
    res.status(201).send(data);
    logger.info("New movie added");
  });
});

movieApp.get("/api/movies/:movieId", (req, res) => {
  movieModel.findById(req.params.movieId, (err, data) => {
    if (err) {
      logger.error(err.message);
      res.send(err.message);
    }
    res.status(201).send(data);
    logger.info("Requested data sent to the user");
  });
});

movieApp.put("/api/movies/:movieId", (req, res) => {
  movieModel.findByIdAndUpdate(req.params.movieId, req.body, (err, data) => {
    if (err) {
      logger.error(err.message);
      res.send(err.message);
    }
    res.status(201).send("Movie Modifieds");
    logger.info("Movie Modified");
  });
});

movieApp.delete("/api/movies/:movieId", (req, res) => {
  movieModel.findByIdAndDelete(req.params.movieId, (err, data) => {
    if (err) {
      logger.error(err.message);
      res.send(err.message);
    }
    res.send("Deleted Sucessfully");
    logger.info("Deleted successfully");
  });
});

module.exports = movieApp;
