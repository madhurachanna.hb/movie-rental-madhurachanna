const express = require("express");
const rentalModel = require("../models/rentals");
const rentalApp = express.Router();

rentalApp.post("/api/rentals", (req, res) => {
  rentalModel.create(req.body, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.status(201).send(data);
  });
});

rentalApp.get("/api/rentals", (req, res) => {
  rentalModel
    .find({})
    .populate("customer movie")
    .exec((err, data) => {
      res.send(data);
    });
});

rentalApp.delete("/api/rentals/:rentalId", (req, res) => {
  movieModel.findByIdAndDelete(req.params.rentalId, (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.send("Deleted Sucessfully");
  });
});

module.exports = rentalApp;
