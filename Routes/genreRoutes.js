const movieModel = require("../models/movie");
const express = require("express");
const genreApp = express.Router();

genreApp.get("/api/genres", (req, res) => {
  movieModel.distinct("genre", (err, data) => {
    if (err) {
      res.send(err.message);
    }
    res.send(data);
  });
});

genreApp.get("/api/genres/:genreName", (req, res) => {
  movieModel.find(
    {
      genre: req.params.genreName
    },
    (err, data) => {
      if (err) {
        res.send(err.message);
      }
      res.send(data);
    }
  );
});

module.exports = genreApp;
