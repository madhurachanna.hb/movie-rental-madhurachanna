let mongoose = require("mongoose");
let Schema = mongoose.Schema;

const genreSchema = new Schema({
  genre: {
    type: String,
    required: [true, "Enter Genre"]
  }
});

module.exports = mongoose.model("genres", genreSchema);
