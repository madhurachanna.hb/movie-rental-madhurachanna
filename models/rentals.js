let mongoose = require("mongoose");
let Schema = mongoose.Schema;

const rentalSchema = new Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "customer"
  },
  movie: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "movies"
  },
  dateIssued: {
    type: Date,
    default: Date.now(),
    required: [true, "Enter date issued"]
  },
  dateReturned: {
    type: Date,
    required: [true, "Enter date returned"]
  },
  rentalFee: {
    type: Number,
    required: [true, "Enter rental fee"]
  }
});

module.exports = mongoose.model("rentals", rentalSchema);
