let mongoose = require("mongoose");
let Schema = mongoose.Schema;

const customersSchema = new Schema({
  name: {
    type: String,
    required: [true, "Enter Customer Name"]
  },
  isPremium: {
    type: Boolean,
    required: [true, "Enter isPremium"]
  },
  phone: {
    type: String,
    required: [true, "Enter Phone Number"]
  }
});

module.exports = mongoose.model("customer", customersSchema);
