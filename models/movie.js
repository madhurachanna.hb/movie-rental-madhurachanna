let mongoose = require("mongoose");
let Schema = mongoose.Schema;
const moviesSchema = new Schema({
  title: {
    type: String,
    required: [true, "Enter Movie Name"]
  },
  genre: {
    type: String,
    required: [true, "Enter Genre"]
  },
  numberInStock: {
    type: Number,
    required: [true, "Enter number in stock"]
  },
  dailyRentalRate: {
    type: Number,
    required: [true, "Enter daily renatal rate"]
  }
});

module.exports = mongoose.model("movies", moviesSchema);
