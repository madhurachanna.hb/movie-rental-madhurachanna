const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();
const movieRoute = require("./Routes/movieRoute");
const customerRotue = require("./Routes/customerRoute");
const genreRoute = require("./Routes/genreRoutes");
const rentalRoute = require("./Routes/rentalRoute");

let url = "mongodb://127.0.0.1:27017/movieDataSet";
mongoose.connect(url, { useNewUrlParser: true });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(rentalRoute);
app.use(movieRoute);
app.use(customerRotue);
app.use(genreRoute);

// if (process.env.NODE_ENV !== 'production') {
//   logger.add(new winston.transports.Console({
//     format: winston.format.simple()
//   }));
// }

app.listen(3000, () => {
  console.log("server started in port 3000");
});
